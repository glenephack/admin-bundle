<?php

namespace GlenEphack\AdminBundle;

use GlenEphack\AdminBundle\DependencyInjection\Compiler\ModulePass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GlenephackAdminBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ModulePass());
    }
}
