<?php

namespace GlenEphack\AdminBundle\Entity;

abstract class Module
{
    public const MODULE_PATH = 'App\\Admin\\';

    public $title;
    public $icon;
    public $moduleName;
    public $isVisible;
    public $isOnlyForAdmins;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title ? $this->title : '';
    }

    /**
     * @param string $title
     *
     * @return Module
     */
    public function setTitle(string $title): Module
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon ? $this->icon : '';
    }

    /**
     * @param string $icon
     *
     * @return Module
     */
    public function setIcon(string $icon): Module
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getModuleName(): string
    {
        return $this->moduleName ? $this->moduleName : '';
    }

    /**
     * @param string $moduleName
     *
     * @return Module
     */
    public function setModuleName(string $moduleName): Module
    {
        $this->moduleName = $moduleName;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->isVisible ? true : false;
    }

    /**
     * @param bool $isVisible
     *
     * @return Module
     */
    public function setIsVisible(bool $isVisible): Module
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOnlyForAdmins(): bool
    {
        return $this->isOnlyForAdmins ? true : false;
    }

    /**
     * @param bool $isOnlyForAdmins
     *
     * @return Module
     */
    public function setIsOnlyForAdmins(bool $isOnlyForAdmins): Module
    {
        $this->isOnlyForAdmins = $isOnlyForAdmins;

        return $this;
    }

    /**
     * @return void
    */
    public function init(): void
    {
        $this->configure();
    }

    /**
     * Function that install module params (describe in project directory in AdminBundle folder)
     *
     * @return void
     */
    protected function configure(): void {}

    /**
     * Function that install module components (describe in project directory in AdminBundle folder)
     *
     * @return void
     */
    protected function initialize(): void {}
}
