<?php

namespace GlenEphack\AdminBundle\Component\Base\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use GlenEphack\AdminBundle\Exception\GlenephackObjectNotFoundException;

/**
 * Class for queries to database (select, insert, delete, update)
*/
abstract class Query
{
    public const ENTITY_NAMESPACE = 'App:';
    public const ENTITY_FIELD_ID  = '.id';

    /**
     * Return a query for output data from database (without limit)
     *
     * @param EntityManager $em
     * @param string        $entity
     * @param array         $params
     *
     * @return array
    */
    public static function getQueryObjects(EntityManager $em, string $entity, array $params = []): QueryBuilder
    {
        $lowerEntity = strtolower($entity);

        $qb = $em->createQueryBuilder();
        $qb
            ->select($lowerEntity)
            ->from(self::ENTITY_NAMESPACE . $entity, $lowerEntity)
        ;

        if ($params) {
            if (isset($params['fields'])) {
                foreach ($params['fields'] as $field) {
                    $qb->innerJoin(
                        self::ENTITY_NAMESPACE . ucfirst($field['entity']),
                        $field['alias'],
                        Join::WITH,
                        $lowerEntity . '.' . $field['alias'] . '=' . $field['alias'] . self::ENTITY_FIELD_ID)
                        ->andWhere($qb->expr()->eq($field['alias'] . self::ENTITY_FIELD_ID, $field['value']))
                    ;
                }
            }

            if (isset($params['search'])) {
                $searchFields = explode(',', $params['search']['fields']);
                $search = $params['search']['value'];

                $expr = [];

                foreach ($searchFields as $field) {
                    $expr[] = $qb->expr()->like($lowerEntity . '.' . $field, $qb->expr()->literal('%' . $search . '%'));
                }

                $qb->andWhere(call_user_func_array([$qb->expr(), 'orX'], $expr));
            }

            if (isset($params['sort'])) {
                $qb->orderBy($lowerEntity . '.' . $params['sort']['field'], $params['sort']['order']);
            }
        }

        return $qb;
    }

    /**
     * Return an any objects from database (product, user, etc)
     *
     * @param QueryBuilder $em
     * @param int          $pageNumber
     * @param int          $limit
     *
     * @return array
    */
    public static function getObjects(QueryBuilder $qb, int $pageNumber, int $limit): array
    {
        $offset = $pageNumber * $limit;

        $qb
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * Returns a pagination for objects
     *
     * @param QueryBuilder $qb
     * @param int          $currentPage
     * @param int          $limit
     *
     * @return array
    */
    public static function getPaginationForObjects(QueryBuilder $qb, int $pageNumber, int $limit): array
    {
        $paginator = new Paginator($qb->getQuery());

        if (count($paginator)) {
            $paginatorCount = count($paginator) - 1;
        } else {
            $paginatorCount = 0;
        }

        $countPages = ceil($paginatorCount / $limit);

        return ['countPages' => $countPages, 'currentPage' => $pageNumber];
    }

    /**
     * Delete objects by ids
     *
     * @param EntityManager $em
     * @param string $entity
     * @param int $id
     *
     * @throws GlenephackObjectNotFoundException
     *
     * @return array
    */
    public static function deleteObjects(EntityManager $em, string $entity, int $id): void
    {
        $object = $em->find(self::ENTITY_NAMESPACE . ucfirst($entity), $id);

        if (!$object) {
            throw new GlenephackObjectNotFoundException();
        }

        $qb = $em->createQueryBuilder();
        $qb
            ->delete()
            ->from(self::ENTITY_NAMESPACE . ucfirst($entity), $entity)
            ->where($qb->expr()->eq($entity . self::ENTITY_FIELD_ID, $id))
            ->getQuery()
            ->execute()
        ;
    }

    /**
     * Returns fields for select2
     *
     * @param EntityManager $em
     * @param string $search
     * @param string $entity
     * @param string $field
     * @param string $alias
     * @param int $limit
     *
     * @return array
    */
    public static function getFields(EntityManager $em, string $search = '', string $entity, string $field, string $alias, int $limit = 0): array
    {
        $qb = $em->createQueryBuilder();
        $qb
            ->select($entity)
            ->from(self::ENTITY_NAMESPACE . ucfirst($entity), $entity)
        ;

        if ($search) {
            $qb->andWhere($qb->expr()->like($entity . '.' . $field, $qb->expr()->literal('%' . $search . '%')));
        }

        $qb->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }
}
