<?php

namespace GlenEphack\AdminBundle\Component\Base\Entity;

/**
 * Class that make the right construction by got fileds (for expampe: Category field)
*/
abstract class Handler
{
    /**
     * Handles objects from database by fields from ModuleTable and crops to 150 letters if it is need
     *
     * @param array $objects
     * @param array $fields
     * @param bool  $isNeedToCrop
     *
     * @return array
    */
    public static function handleObjectsByFields(array $objects, array $fields, bool $isNeedToCrop = false): array
    {
        $records = [];
        foreach ($objects as $key => $object) {
            $records[$key]['id'] = $object->getId();

            foreach ($fields as $field => $value) {
                $method = 'get' . ucfirst($field);

                switch ($value['type']) {
                    case 'int':
                        $records[$key][$field] = call_user_func([$object, $method], $method);

                        break;

                    case 'text':
                        $records[$key][$field] = call_user_func([$object, $method], $method);

                        break;

                    case 'object':
                        $relationMethod = 'get' . ucfirst($value['field']);
                        $relationObject = call_user_func([$object, $method], $method);
                        $records[$key][$field] = call_user_func([$relationObject, $relationMethod], $relationMethod);

                        break;

                    case 'timestamp':
                        $records[$key][$field] = date('d.m.Y H:i', call_user_func([$object, $method], $method));

                        break;
                }
            }
        }

        $result = [];
        if ($isNeedToCrop) {
            foreach ($records as $record) {
                $fields = [];

                foreach ($record as $field) {
                    $fields[] = mb_strcut($field, 0, 150);
                }
                $result[] = $fields;
            }
        } else {
            $result = $records;
        }

        return $result;
    }
}
