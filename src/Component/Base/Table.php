<?php

namespace GlenEphack\AdminBundle\Component\Base;

use Doctrine\ORM\EntityManager;
use GlenEphack\AdminBundle\Component\Base\Entity\Handler;
use GlenEphack\AdminBundle\Component\Base\Entity\Query;

abstract class Table
{
    /**
     * Fields that you will search in the database
     *
     * @var array
    */
    protected $searchFields;

    /**
     * Names of fields of the table
     *
     * @var array
    */
    protected $fieldNames;

    /**
     * Fields of table from Entity
     *
     * @var array
    */
    protected $fields;
    
    /**
     * Additional filters for table
     * 
     * @var array
    */
    protected $filters;
    
    /**
     * Select any sorts by field in database
     * 
     * @var array
    */
    protected $sorts;
    
    /**
     * Maximum quantity data
     * 
     * @var integer;
    */
    protected $limit;

    /**
     * @return array || null
     */
    public function getSearchFields(): ?array
    {
        return $this->searchFields;
    }

    /**
     * @param array $searchFields
     *
     * @return Table
     */
    public function setSearchFields(array $searchFields): Table
    {
        $this->searchFields = $searchFields;

        return $this;
    }

    /**
     * @return array
     */
    public function getFieldNames(): array
    {
        return $this->fieldNames;
    }

    /**
     * @param array $fieldNames
     *
     * @return Table
     */
    public function setFieldNames(array $fieldNames): Table
    {
        $this->fieldNames = $fieldNames;

        return $this;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     *
     * @return Table
     */
    public function setFields(array $fields): Table
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @return array || null
     */
    public function getFilters(): ?array
    {
        return $this->filters;
    }

    /**
     * @param array $filters
     *
     * @return Table
     */
    public function setFilters(array $filters): Table
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @return array || null
     */
    public function getSorts(): ?array
    {
        return $this->sorts;
    }

    /**
     * @param array $sorts
     *
     * @return Table
     */
    public function setSorts(array $sorts): Table
    {
        $this->sorts = $sorts;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit ? $this->limit : 15;
    }

    /**
     * @param int $limit
     *
     * @return Table
     */
    public function setLimit(int $limit): Table
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Method that get and set data by fields of Entity
     *
     * @return void
    */
    public function initData(): void {}

    /**
     * Method that get data from database
     *
     * @param EntityManager $em
     * @param bool          $isNewModule
     * @param string        $entity
     * @param array         $params
     *
     * @return array
    */
    public function initialize(EntityManager $em, string $entity, bool $isNewModule, array $params = []): array
    {
        $this->initData();

        $fieldNames = $this->getFieldNames();
        array_push($fieldNames, 'Действия');

        $result = [];
        $pageNumber = $isNewModule ? 0 : $params['pageNumber'];
        $searchFields = implode(',', $this->getSearchFields());
        if ($isNewModule) {
            $result = [
                'pageNumber'   => $pageNumber,
                'filters'      => $this->getFilters(),
                'sorts'        => $this->getSorts(),
                'limit'        => $this->getLimit(),
                'fields'       => json_encode($this->getFields())
            ];
        }

        if (!$isNewModule) {
            $params['search']['fields'] = $searchFields;
        }

        $qb         = Query::getQueryObjects($em, $entity, $params);
        $objects    = Query::getObjects($qb, $pageNumber, $this->getLimit());

        $result['records']    = Handler::handleObjectsByFields($objects, $this->getFields(), true);
        $result['pagination'] = Query::getPaginationForObjects($qb, $pageNumber, $this->getLimit());

        $result['fieldNames'] = $fieldNames;

        return $result;
    }
}
