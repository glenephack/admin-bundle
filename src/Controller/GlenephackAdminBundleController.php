<?php

namespace GlenEphack\AdminBundle\Controller;

use Doctrine\ORM\EntityManager;
use GlenEphack\AdminBundle\Component\Base\Entity\Handler;
use GlenEphack\AdminBundle\Component\Base\Entity\Query;
use GlenEphack\AdminBundle\Entity\Module;
use GlenEphack\AdminBundle\Exception\GlenephackModuleAccessDeniedException;
use GlenEphack\AdminBundle\Exception\GlenephackModuleHttpException;
use GlenEphack\AdminBundle\Exception\GlenephackModuleNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GlenephackAdminBundleController extends AbstractController
{
    /**
     * @Route(path="/", name="glenephack_admin_index")
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('@GlenephackAdminBundle/home.html.twig', ['moduleList' => $this->getModuleList(), 'module' => null]);
    }

    /**
     * @Route(path="/module/{template}/", name="glenephack_module_page")
     *
     * @param Request
     *
     * @throws GlenephackModuleNotFoundException
     * @throws GlenephackModuleAccessDeniedException
     * @throws GlenephackModuleHttpException
     *
     * @return Response
     */
    public function module(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $moduleManager = $this->container->get('glenephack_admin');

        $moduleName = $request->get('template') ? ucfirst($request->get('template')) : '';
        $module = $moduleManager->getModule($moduleName);

        /**
         * @var $module Module
         */
        if (!$module) {
            throw new GlenephackModuleNotFoundException();
        }

        if ($module->isOnlyForAdmins() && !$this->isGranted('ROLE_ADMIN')) {
            throw new GlenephackModuleAccessDeniedException();
        }

        $moduleTableClass = $module::MODULE_PATH . $moduleName . '\\' . $moduleName . 'Table';

        if (!class_exists($moduleTableClass)) {
            throw new GlenephackModuleHttpException();
        }

        $moduleTable = new $moduleTableClass;

        if ($request->isXmlHttpRequest()) {
            $filters = [
                'fields'     => $request->request->get('filters'),
                'sort'       => $request->request->get('sort'),
                'search'     => $request->request->get('search'),
                'pageNumber' => $request->request->get('page_number')
            ];

            $moduleData = $moduleTable->initialize($em, $moduleName, false, $filters);

            return $this->render('@GlenephackAdminBundle/module/table/table.html.twig', [
                'records'        => $moduleData['records'],
                'pagination'     => $moduleData['pagination'],
                'fieldNames'     => $moduleData['fieldNames']
            ]);
        } else {
            $moduleData = $moduleTable->initialize($em, $moduleName, true);
            $moduleData['moduleName'] = $moduleName;

            return $this->render('@GlenephackAdminBundle/home.html.twig', ['moduleList' => $this->getModuleList(), 'module' => $moduleData]);
        }
    }

    /**
     * @Route(path="/search/entity/fields/", name="glenephack_search_entity_fields", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getSearchFields(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $search = $request->request->get('search', '');
        $entity = $request->request->get('entity');
        $field  = $request->request->get('field');
        $alias  = $request->request->get('alias');
        $limit  = $request->request->get('limit', 15);

        $searchObjects = Query::getFields($em, $search, $entity, $field, $alias, $limit);

        $result = [];
        foreach ($searchObjects as $object) {
            $method = 'get' . ucfirst($field);
            $result[] = [
                'id'     => $object->getId(),
                'text'   => call_user_func([$object, $method], $method),
                'entity' => $entity,
                'alias'  => $alias
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * @return array
    */
    public function getModuleList(): array
    {
        $moduleManager = $this->container->get('glenephack_admin');
        $moduleList = $moduleManager->getModules();

        $result = [];
        /** @var $module Module */
        foreach ($moduleList as $module) {
            if ($module->isVisible()) {
                if (($module->isOnlyForAdmins() && $this->isGranted('ROLE_ADMIN')) || !$module->isOnlyForAdmins()) {
                    $result[] = [
                        'name'      => $module->getModuleName(),
                        'title'     => $module->getTitle(),
                        'icon'      => $module->getIcon(),
                        'isVisible' => $module->isVisible(),
                        'isActive'  => false
                    ];
                }
            }
        }

        return $result;
    }

    /**
     * @Route(path="/delete/object/", name="glenephack_delete_object", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws GlenephackModuleAccessDeniedException
     *
     * @return Response
    */
    public function deleteObject(Request $request): JsonResponse
    {
        $id = $request->request->get('object_id');
        $entity = $request->request->get('object_entity');

        if (!$this->isGranted('ROLE_ADMIN')) {
            throw new GlenephackModuleAccessDeniedException();
        }

        Query::deleteObjects($this->getDoctrine()->getManager(), $entity, $id);

        $response = [
            'status'  => 'success',
            'message' => 'Объект успешно удален'
        ];

        return new JsonResponse($response);
    }
}
