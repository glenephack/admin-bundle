<?php

namespace GlenEphack\AdminBundle\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GlenephackObjectNotFoundException extends NotFoundHttpException
{
    public function __construct(string $message = null, \Throwable $previous = null, int $code = 0, array $headers = [])
    {
        parent::__construct('Объект не найден', $previous, $code, $headers);
    }
}
