<?php

namespace GlenEphack\AdminBundle\Exception;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class GlenephackModuleAccessDeniedException extends AccessDeniedHttpException
{
    public function __construct(string $message = null, \Throwable $previous = null, int $code = 0, array $headers = [])
    {
        parent::__construct('Доступ к модулю запрещен', $previous, $code, $headers);
    }
}
