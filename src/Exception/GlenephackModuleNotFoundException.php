<?php

namespace GlenEphack\AdminBundle\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GlenephackModuleNotFoundException extends NotFoundHttpException
{
    public function __construct(string $message = null, \Throwable $previous = null, int $code = 0, array $headers = [])
    {
        parent::__construct('Модуль не найден', $previous, $code, $headers);
    }
}
