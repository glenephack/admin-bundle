<?php

namespace GlenEphack\AdminBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class GlenephackModuleHttpException extends HttpException
{
    public function __construct(int $statusCode = 500, string $message = null, \Throwable $previous = null, array $headers = [], ?int $code = 0)
    {
        parent::__construct(500, 'Внутренняя ошибка сервера');
    }
}
