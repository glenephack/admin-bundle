$(function () {
    $(document).on('click', '.record-delete', function () {
        let $this = $(this);
        let data = {
            object_id: $this.parents('.table-record').find('.record-field-id').text(),
            object_entity: $('.module-name span').data('entity')
        };

        $.confirm({
            title: 'Удалить',
            content: 'Вы уверены?',
            buttons: {
                confirmButton: {
                    text: 'Да',
                    btnClass: 'btn-primary',
                    action: function () {
                        $.ajax({
                            url: $this.parent().data('route'),
                            method: 'post',
                            data: data,
                            dataType: 'json',
                            beforeSend: function () {
                                $('.module-block-table').mask();
                            },
                            success: function (response) {
                                $this.parents('.table-record').remove();
                                $.notify(response['message'], {type: response['status']});
                            },
                            error: function (requestObject, error, errorThrown) {
                                $.notify(requestObject.statusText, {type: 'danger'});
                            },
                            complete: function () {
                                $('.module-block-table').unmask();
                            }
                        });
                    }
                },
                cancelButton: {
                    text: 'Нет'
                }
            }
        });
    });

    $(document).on('click', '.action-add', function (e) {
        e.preventDefault();

        alert(123);
    });
});
$(function () {
    let $select2Inputs = $('.select2');

    $.each($select2Inputs, function (index, item) {
        let $select2 = $(item);
        let data = {
            entity: $select2.data('entity'),
            field: $select2.data('field'),
            limit: $select2.data('limit'),
            alias: $select2.data('alias')
        };

        $select2.select2({
            ajax: {
                url: $select2.data('ajax-url'),
                method: 'post',
                delay: 250,
                data: function (search) {
                    data.search = search.term;

                    return data;
                },
                dataType: 'json',
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });
    });

    let $select2HasData = $('.select2-has-data');

    $select2HasData.select2();
});
$(function () {
    $(document).on('change', '.form-select', function () {
        $('#form-module-table').trigger('submit');
    });

    $(document).on('submit', '#form-module-table', function (e) {
        e.preventDefault();

        renderTableAjax($(this), getFilters());
    });

    // Improve this event (too many conditions)
    $(document).on('click', '.page-item', function (e) {
        e.preventDefault();

        let $this = $(this);

        if (!$this.hasClass('disabled') || $this.hasClass('active')) {
            let $activePage = $this.parent().find('.active');

            if ($this.hasClass('page-item-prev') || $this.hasClass('page-item-next')) {
                if (!$this.hasClass('disabled')) {
                    if ($this.hasClass('page-item-prev')) {
                        let $prevPage = $activePage.prev();

                        if ($activePage.next().hasClass('page-item-next') && $activePage.next().hasClass('disabled')) {
                            $activePage.next().removeClass('disabled');
                        }

                        if ($prevPage.prev().hasClass('page-item-prev')) {
                            $this.addClass('disabled');
                        }

                        $prevPage.addClass('active');
                        $activePage.removeClass('active');
                    } else {
                        let $nextPage = $activePage.next();

                        if ($activePage.prev().hasClass('page-item-prev') && $activePage.prev().hasClass('disabled')) {
                            $activePage.prev().removeClass('disabled');
                        }

                        if ($nextPage.next().hasClass('page-item-next')) {
                            $this.addClass('disabled');
                        }

                        $nextPage.addClass('active');
                        $activePage.removeClass('active');
                    }
                }
            } else if (!$this.hasClass('active')) {
                $activePage.removeClass('active');
                $this.addClass('active');

                if ($this.next().hasClass('page-item-next')) {
                    $this.next().addClass('disabled');
                } else {
                    $('.page-item-next').removeClass('disabled');
                }

                if ($this.prev().hasClass('page-item-prev')) {
                    $this.prev().addClass('disabled');
                } else {
                    $('.page-item-prev').removeClass('disabled');
                }
            }

            renderTableAjax($('#form-module-table'), getFilters());
        }
    });

    function getFilters() {
        let data = {
            search: {
                value: $('.filter-search').val()
            },
            page_number: $('.page-item.active').data('page-number')
        };

        if ($('.select2-has-data').val()) {
            data.sort = {
                field: $('.select2-has-data').val(),
                order: $('.select2-has-data').find(":selected").data('order')
            };
        }

        let $select2Fields = $('.select2-filters');

        let filters = [];
        $.each($select2Fields, function (index, item) {
            if ($(item).val()) {
                let $filterData = {
                    entity: $(item).data('entity'),
                    alias: $(item).data('alias'),
                    value: $(item).val()
                };

                filters.push($filterData);
            }
        });

        data.filters = filters;

        return data;
    }

    function renderTableAjax($form, data) {
        let $moduleBlockTable = $('.module-block-table');

        $.ajax({
            url: $form.attr('action'),
            method: $form.attr('method'),
            data: data,
            beforeSend: function () {
                $moduleBlockTable.mask();
            },
            success: function (response) {
                $moduleBlockTable.empty();
                $moduleBlockTable.append(response);
                $moduleBlockTable.unmask();

                $.notify('Данные успешно обновлены', {type: 'success'});
            }
        });
    }
});