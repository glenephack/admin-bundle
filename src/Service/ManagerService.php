<?php

namespace GlenEphack\AdminBundle\Service;

use GlenEphack\AdminBundle\Entity\Module;

class ManagerService
{
    /**
     * Property that is saving modules (ExampleModule extends Module)
    */
    private $modules = [];

    /**
     * @return Module
    */
    public function getModule(string $name): ?Module
    {
        return isset($this->modules[$name]) ? $this->modules[$name] : null;
    }

    /**
     * @return array
    */
    public function getModules(): array
    {
        return $this->modules ? $this->modules : [];
    }

    /**
     * @param Module $module
     *
     * @return void
    */
    public function addModule(Module $module): void
    {
        $module->init();
        $this->modules[$module->getModuleName()] = $module;
    }
}
