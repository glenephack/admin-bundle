<?php

namespace GlenEphack\AdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('glenephack_admin');
        $rootNode
            ->children()
                ->scalarNode('title')->isRequired()->end()
                ->scalarNode('icon')->end()
                ->variableNode('modules')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
