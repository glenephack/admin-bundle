var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var del = require('del');
var path = require('path');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');

var publicPath = 'src/Resources/public/';

gulp.task('build-vendor-css', function() {
    return gulp.src(['assets/css/helpers/*.scss',
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/mdi/css/materialdesignicons.css',
        'bower_components/select2/dist/css/select2.css',
        'node_modules/jquery-confirm/css/jquery-confirm.css'])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest(publicPath + 'css'));
});

gulp.task('build-vendor-js', function() {
    return gulp.src(['bower_components/jquery/dist/jquery.js',
        'bower_components/jquery-form/jquery.form.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/handlebars/handlebars.js',
        'bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.js',
        'bower_components/select2/dist/js/select2.js',
        'node_modules/jquery-confirm/js/jquery-confirm.js',
        'assets/js/core/*.js'])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(publicPath + 'js'));
});

gulp.task('build-css', function() {
    return gulp.src(['assets/css/modules/**/*.scss', 'assets/css/layout/*.scss'])
        .pipe(sass())
        .pipe(concat('common.css'))
        .pipe(gulp.dest(publicPath + 'css'));
});

gulp.task('build-js', function() {
    return gulp.src('assets/js/*.js')
        .pipe(concat('common.js'))
        .pipe(gulp.dest(publicPath + 'js'));
});

gulp.task('build-fonts', function() {
    return gulp.src(['assets/fonts/**/*.{woff,ttf,woff2,eot}', 'bower_components/mdi/fonts/*.{woff,ttf,woff2,eot}'])
        .pipe(gulp.dest(publicPath + 'fonts'));
});

gulp.task('build-handlebars', function() {
    return gulp.src('assets/handlebars/**/*.hbs')
        .pipe(handlebars())
        .pipe(wrap('Handlebars.template(<%= contents %>)'))
        .pipe(declare({
            namespace: 'hbs.templates',
            noRedeclare: true
        }))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest(publicPath + 'js'));
});


gulp.task('watch', function () {
    gulp.watch('assets/css/**/*.scss', gulp.series('build-css'));
    gulp.watch('assets/js/*.js', gulp.series('build-js'));
    gulp.watch('assets/js/core/*.js', gulp.series('build-vendor-js'));
    gulp.watch('assets/handlebars/**/*.hbs', gulp.series('build-handlebars'));

    /*var $fontsWatcher = gulp.watch('assets/fonts/!**!/!*.{ttf,woff,woff2,eot,svg}', gulp.series('build-fonts'));

    $fontsWatcher.on('unlink', function(filepath) {
        var filePathFromSrc = path.relative(path.resolve('assets'), filepath);
        var destFilePath = path.resolve('../public', filePathFromSrc);

        del.sync(destFilePath, {force: true});
    });*/
});

gulp.task('external', gulp.series(
    'build-css', 'build-js', 'build-vendor-js',
    'build-vendor-css', 'build-fonts', 'build-handlebars', 'watch'));
