$(function () {
    let $select2Inputs = $('.select2');

    $.each($select2Inputs, function (index, item) {
        let $select2 = $(item);
        let data = {
            entity: $select2.data('entity'),
            field: $select2.data('field'),
            limit: $select2.data('limit'),
            alias: $select2.data('alias')
        };

        $select2.select2({
            ajax: {
                url: $select2.data('ajax-url'),
                method: 'post',
                delay: 250,
                data: function (search) {
                    data.search = search.term;

                    return data;
                },
                dataType: 'json',
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });
    });

    let $select2HasData = $('.select2-has-data');

    $select2HasData.select2();
});