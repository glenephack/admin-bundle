$(function () {
    $(document).on('click', '.record-delete', function () {
        let $this = $(this);
        let data = {
            object_id: $this.parents('.table-record').find('.record-field-id').text(),
            object_entity: $('.module-name span').data('entity')
        };

        $.confirm({
            title: 'Удалить',
            content: 'Вы уверены?',
            buttons: {
                confirmButton: {
                    text: 'Да',
                    btnClass: 'btn-primary',
                    action: function () {
                        $.ajax({
                            url: $this.parent().data('route'),
                            method: 'post',
                            data: data,
                            dataType: 'json',
                            beforeSend: function () {
                                $('.module-block-table').mask();
                            },
                            success: function (response) {
                                $this.parents('.table-record').remove();
                                $.notify(response['message'], {type: response['status']});
                            },
                            error: function (requestObject, error, errorThrown) {
                                $.notify(requestObject.statusText, {type: 'danger'});
                            },
                            complete: function () {
                                $('.module-block-table').unmask();
                            }
                        });
                    }
                },
                cancelButton: {
                    text: 'Нет'
                }
            }
        });
    });

    $(document).on('click', '.action-add', function (e) {
        e.preventDefault();

        alert(123);
    });
});