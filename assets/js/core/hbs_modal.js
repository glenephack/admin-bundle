$(document).on('hidden.bs.modal', function () {
    $('.modal').remove();
});

var HbsModal = function (template, params) {
    this.template = template;
    this.params = params;

    this.init = function () {
        if (!$('.modal').length) {
            $('.body').append(hbs['templates'][this.template](this.params));
            $('.modal').modal();
        }
    };

    this.destroy = function () {
        $('.modal').modal('hide');
    };
};